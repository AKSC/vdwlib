#include <string>
#include <vector>
#include <map>
#include <openbabel/babelconfig.h>
#if (OB_VERSION >= OB_VERSION_CHECK(3, 0, 0))
    #include <openbabel/elements.h>
    #define OBELEMENTS(EXPRESSION) OpenBabel::OBElements::EXPRESSION
#else
    #include <openbabel/data.h>
    #define OBELEMENTS(EXPRESSION) OpenBabel::OBElementTable().EXPRESSION
#endif
#include "vdwlib/radii.hpp"
namespace vdwlib {
    static std::string _cur_radii_src("Bondi");
    static bool _use_bondi_h = false;
    static double _cur_radii_scale = 1.;
    static std::map<std::string, std::vector<double>> _radii_tables = {
        #undef RADII_TABLE
        #define RADII_TABLE(SYMBOL, BONDI, CAVALLO, BATSANOV, ALVAREZ, CRC) BONDI
        {"Bondi", {
            #include "radii-table.h"
        }},
        #undef RADII_TABLE
        #define RADII_TABLE(SYMBOL, BONDI, CAVALLO, BATSANOV, ALVAREZ, CRC) CAVALLO
        {"Cavallo", {
            #include "radii-table.h"
        }},
        #undef RADII_TABLE
        #define RADII_TABLE(SYMBOL, BONDI, CAVALLO, BATSANOV, ALVAREZ, CRC) BATSANOV
        {"Batsanov", {
            #include "radii-table.h"
        }},
        #undef RADII_TABLE
        #define RADII_TABLE(SYMBOL, BONDI, CAVALLO, BATSANOV, ALVAREZ, CRC) ALVAREZ
        {"Alvarez", {
            #include "radii-table.h"
        }},
        #undef RADII_TABLE
        #define RADII_TABLE(SYMBOL, BONDI, CAVALLO, BATSANOV, ALVAREZ, CRC) CRC
        {"CRC", {
            #include "radii-table.h"
        }},
    };
    static std::map<std::string, double> _bondi_h_radii = {
        #include "radii-bondi-h.h"
    };
    static std::vector<double> *_cur_radii_table = &_radii_tables[_cur_radii_src];
    static double _cur_bondi_h_radius = _bondi_h_radii[_cur_radii_src];

    bool set_radii_source(std::string radii_src, bool use_bondi_h) {
        if (_radii_tables.find(radii_src) != _radii_tables.end() || radii_src == "OB") {
            _cur_radii_src = radii_src;
            _use_bondi_h = use_bondi_h;
            if (radii_src == "OB") {
                _cur_radii_table = nullptr;
            } else {
                _cur_radii_table = &_radii_tables[_cur_radii_src];
            };
            _cur_bondi_h_radius = _bondi_h_radii[_cur_radii_src];
            return true;
        } else {
            return false;
        };
    };

    std::string get_radii_source() {
        return  _cur_radii_src;
    };

    std::vector<std::string> get_radii_sources() {
        std::vector<std::string> sources;
        for (const auto& column : _radii_tables) {
            sources.push_back(column.first);
        };
        sources.push_back("OB");
        return sources;
    };

    bool use_bondi_h() {
        return _use_bondi_h;
    };

    bool set_radii_scale(double scale) {
        if (scale <= 0) {
            return false;
        };
        _cur_radii_scale = scale;
        return true;
    };

    double get_radii_scale() {
        return _cur_radii_scale;
    };

    double get_radius(unsigned atomic_num) {
        if (atomic_num == 1 && _use_bondi_h) {
            return _cur_bondi_h_radius * _cur_radii_scale;
        };
        if (_cur_radii_src == "OB") {
            return OBELEMENTS(GetVdwRad(atomic_num)) * _cur_radii_scale;
        } else {
            if (atomic_num  > 0 && atomic_num < _cur_radii_table->size()) {
                return (*_cur_radii_table)[atomic_num - 1] * _cur_radii_scale;
            } else {
                return 0.00;
            };
        };
    };

    double get_radius(const OpenBabel::OBAtom& atom) {
        return get_radius(atom.GetAtomicNum());
    };

    double get_radius(const OpenBabel::OBAtom *atom) {
        return get_radius(atom->GetAtomicNum());
    };
};

