#include "common.hpp"

namespace vdwlib {
    OpenBabel::vector3 min(OpenBabel::vector3 a, OpenBabel::vector3 b) {
        auto output = OpenBabel::vector3();
        output.SetX((a.GetX() < b.GetX()) ? a.GetX() : b.GetX());
        output.SetY((a.GetY() < b.GetY()) ? a.GetY() : b.GetY());
        output.SetZ((a.GetZ() < b.GetZ()) ? a.GetZ() : b.GetZ());
        return output;
    };

    OpenBabel::vector3 max(OpenBabel::vector3 a, OpenBabel::vector3 b) {
        auto output = OpenBabel::vector3();
        output.SetX((a.GetX() > b.GetX()) ? a.GetX() : b.GetX());
        output.SetY((a.GetY() > b.GetY()) ? a.GetY() : b.GetY());
        output.SetZ((a.GetZ() > b.GetZ()) ? a.GetZ() : b.GetZ());
        return output;
    };

    OpenBabel::vector3 gen_perp(OpenBabel::vector3& inp) {
        OpenBabel::vector3 out;
        if (std::abs(inp.GetZ()) < std::abs(inp.GetX())) {
            out = OpenBabel::vector3(inp.GetY(), -inp.GetX(), 0);
        } else {
            out = OpenBabel::vector3(0, -inp.GetZ(), inp.GetY());
        };
        out.normalize();
        return out;
    };

    void print_progress_bar(double progress, std::ostream& stream) {
        unsigned percentage = static_cast<unsigned>(100 * progress);
        stream << "[" << std::string(percentage/2, '=') 
                      << std::string(50 - percentage / 2, ' ') << "] "
                      << percentage << "%\r";
        stream.flush();
    };
};

