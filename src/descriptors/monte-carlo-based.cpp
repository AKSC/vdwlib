#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <random>
#include <numeric>
#include <algorithm>
#include <omp.h>
#include "vdwlib/radii.hpp"
#include "vdwlib/descriptors/monte-carlo-based.hpp"
#include "common.hpp"

namespace vdwlib::descriptors::monte_carlo_based {
    static long _cur_sample_size = 1e6;
    static int _cur_seed = 0;
    static std::uniform_real_distribution<double> _uniform_distribution(0., 1.);
    static std::normal_distribution<double> _normal_distribution(0., 1.);
    
    class OverlapIntegrator {
        public:
            OverlapIntegrator() {};

            void add_sphere(int group_id, Sphere& sphere) {
                if (group_id >= _spheres.size()) {
                    _spheres.resize(group_id + 1);
                };
                _spheres[group_id].push_back(sphere);
            };

            void add_sphere(int group_id, OpenBabel::vector3& position,
                            double radius) {
                Sphere sphere(position, radius);
                add_sphere(group_id, sphere);
            };
            
            double compute() {
                _compute_distributions();
                long overlap_counter = 0;
                #pragma omp parallel for reduction(+: overlap_counter) default(none) shared(_cur_sample_size, _cur_seed, _normal_distribution, _uniform_distribution)
                for (int i_sample = 0; i_sample < _cur_sample_size; ++i_sample) {
                    std::random_device  rng_engine;
                    //rng_engine.seed(_cur_seed + omp_get_thread_num());
                    int i_group = _sphere_group_distribution(rng_engine);
                    int i_sphere = _sphere_distribution[i_group](rng_engine);
                    OpenBabel::vector3 pos_1(_spheres[i_group][i_sphere].GetPosition()); 
                    double radius_1 = _spheres[i_group][i_sphere].GetRadius();
                    // Random point in sphere based on reference:
                    // https://math.stackexchange.com/a/87238
                    OpenBabel::vector3 sample_pos(
                        _normal_distribution(rng_engine),
                        _normal_distribution(rng_engine),
                        _normal_distribution(rng_engine)
                    );
                    sample_pos.normalize();
                    sample_pos *= radius_1;
                    sample_pos *= std::cbrt(_uniform_distribution(rng_engine));
                    sample_pos += pos_1;
                    
                    for (int j_sphere = 0; j_sphere < _spheres[i_group].size(); ++j_sphere) {
                        if (j_sphere == i_sphere) {
                            continue;
                        };
                        if (!_spheres[i_group][i_sphere].PositionInSphere(sample_pos, _spheres[i_group][j_sphere])) {
                            goto skip_sample;
                        };
                    };
                    for (int j_group = 0; j_group < _spheres.size(); ++j_group) {
                        if (j_group == i_group) {
                            continue;
                        };
                        if (std::none_of(
                            _spheres[j_group].begin(), _spheres[j_group].end(),
                            [&](auto& sphere){return sphere.PositionInSphere(sample_pos);})
                        ) {
                            goto skip_sample;
                        };
                    };
                    ++overlap_counter;

                    skip_sample:;
                    //std::cout << "(" << pos.GetX() << ", " << pos.GetY() << ", " << pos.GetZ() << ") " << i_group << '|' << i_sphere << '|' << overlap_counter << std::endl;
                };
                return static_cast<double>(overlap_counter) / _cur_sample_size * _total_volume;
            };

        protected:
            std::discrete_distribution<int> _sphere_group_distribution;
            std::vector<std::discrete_distribution<int>> _sphere_distribution;
            std::vector<std::vector<Sphere>> _spheres;
            double _total_volume;

            void _compute_distributions() {
                int n_groups = _spheres.size();
                _sphere_distribution = std::vector<std::discrete_distribution<int>>(n_groups);
                std::vector<double> group_volumes(n_groups, 0);
                for (int i_group = 0; i_group < n_groups; ++i_group) {
                    int n_spheres = _spheres[i_group].size();
                    std::vector<double> sphere_volumes(n_spheres, 0);
                    for (int i_sphere = 0; i_sphere < n_spheres; ++i_sphere) {
                        Sphere sphere = _spheres[i_group][i_sphere];
                        double volume = sphere.GetVolume();
                        sphere_volumes[i_sphere] = volume;
                        group_volumes[i_group] += volume;
                    };
                    _sphere_distribution[i_group] = std::discrete_distribution<int>(sphere_volumes.begin(), sphere_volumes.end());
                };
                _sphere_group_distribution = std::discrete_distribution<int>(group_volumes.begin(), group_volumes.end());
                _total_volume = std::accumulate(group_volumes.begin(), group_volumes.end(), 0.) / n_groups;
            };
    };

    class SurfaceIntegrator {
        public:
            void add_sphere(Sphere& sphere) {
                _spheres.push_back(sphere);
            };

            void add_inaccesible_volume_sphere(Sphere& sphere) {
                _inaccesible_volume_spheres.push_back(sphere);
            };

            double compute() {
                int n_spheres = _spheres.size();
                if (n_spheres == 0) {
                    return 0.;
                };
                double total_area = 0;
                #pragma omp parallel for reduction(+: total_area) default(none) shared(n_spheres)
                for (int i_sphere = 0; i_sphere < n_spheres; ++i_sphere) {
                    total_area += _spheres[i_sphere].GetSurface();
                };
                long surface_counter = 0;
                std::uniform_int_distribution<> sphere_idx_distribution(0, n_spheres - 1);
                #pragma omp parallel for reduction(+: surface_counter) default(none) shared(n_spheres, _cur_sample_size, _cur_seed, _normal_distribution, sphere_idx_distribution)
                for (int i_sample = 0; i_sample < _cur_sample_size; ++i_sample) {
                    std::random_device  rng_engine;
                    int i_sphere = sphere_idx_distribution(rng_engine);
                    OpenBabel::vector3 center(_spheres[i_sphere].GetPosition()); 
                    double radius = _spheres[i_sphere].GetRadius();
                    // Random point on sphere based on reference:
                    // https://math.stackexchange.com/a/87238
                    OpenBabel::vector3 sample_pos(
                        _normal_distribution(rng_engine),
                        _normal_distribution(rng_engine),
                        _normal_distribution(rng_engine)
                    );
                    sample_pos.normalize();
                    sample_pos *= radius;
                    sample_pos += center;

                    for (int j_sphere = 0; j_sphere < n_spheres; ++j_sphere) {
                        if (i_sphere == j_sphere) {
                            continue;
                        };
                        if (_spheres[j_sphere].PowerOfPosition(sample_pos) < 0) {
                            goto skip_sample;
                        };
                    };
                    for (auto other_sphere : _inaccesible_volume_spheres) {
                        if (other_sphere.PowerOfPosition(sample_pos) < 0) {
                            goto skip_sample;
                        };
                    };
                    ++surface_counter;

                    skip_sample:;
                };
                return static_cast<double>(surface_counter) / _cur_sample_size * total_area;
            };

        protected:
            std::vector<Sphere> _spheres;
            std::vector<Sphere> _inaccesible_volume_spheres;
    };

    bool set_sample_size(long sample_size) {
        if (sample_size > 0) {
            _cur_sample_size = sample_size;
            return true;
        } else {
            return false;
        };
    };

    long get_sample_size() {
        return _cur_sample_size;
    };

    bool set_seed(int seed) {
        if (seed > 0) {
            _cur_seed = seed;
            return true;
        } else {
            return false;
        };
    };

    int get_seed() {
        return _cur_seed;
    };

    double get_volume(const OpenBabel::OBMol& molecule) {
        if (molecule.NumAtoms() == 0) {
            return 0;
        };
        OverlapIntegrator overlap_integrator;
        for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
            OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
            OpenBabel::vector3 position = atom->GetVector();
            double radius = vdwlib::get_radius(atom);
            overlap_integrator.add_sphere(0, position, radius);
        };
        return overlap_integrator.compute();
    };

    double get_vdwo(const OpenBabel::OBMol& receptor,
        const OpenBabel::OBMol& ligand
    ) {
        if (ligand.NumAtoms() == 0 || receptor.NumAtoms() == 0) {
            return 0;
        };
        OverlapIntegrator overlap_integrator;
        for (unsigned group_id = 0; group_id < 2; ++group_id) {
            OpenBabel::OBMol molecule = (group_id == 0) ? receptor : ligand;
            for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
                OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
                OpenBabel::vector3 position = atom->GetVector();
                double radius = vdwlib::get_radius(atom);
                overlap_integrator.add_sphere(group_id, position, radius);
            };
        };
        return overlap_integrator.compute();
    };

    double get_burv_abs(const OpenBabel::OBMol& molecule,
        const OpenBabel::vector3 origin, double radius
    ) {
        if (molecule.NumAtoms() == 0) {
            return 0;
        };
        OverlapIntegrator overlap_integrator;
        vdwlib::Sphere sphere(origin, radius);
        overlap_integrator.add_sphere(0, sphere);
        for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
            OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
            OpenBabel::vector3 position = atom->GetVector();
            double radius = vdwlib::get_radius(atom);
            overlap_integrator.add_sphere(1, position, radius);
        };
        return overlap_integrator.compute();
    };

    double get_burv_abs(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin, double radius
    ) {
        if (exclude_origin) {
            OpenBabel::OBMol copy = OpenBabel::OBMol(molecule);
            OpenBabel::OBAtom *atom = copy.GetAtom(origin);
            OpenBabel::vector3 position = atom->GetVector();
            copy.DeleteAtom(atom);
            return get_burv_abs(copy, position, radius);
        } else {
            OpenBabel::OBAtom *atom = molecule.GetAtom(origin);
            OpenBabel::vector3 position = atom->GetVector();
            return get_burv_abs(molecule, position, radius);
        };
    };

    double get_burv(const OpenBabel::OBMol& molecule,
         const OpenBabel::vector3 origin, double radius
    ) {
        vdwlib::Sphere sphere(origin, radius);
        return 100. * get_burv_abs(molecule, origin, radius
                                   ) / sphere.GetVolume();
    };

    double get_burv(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin, double radius
    ) {
        vdwlib::Sphere sphere(origin, radius);
        return 100. * get_burv_abs(molecule, origin, exclude_origin, radius
                                   ) / sphere.GetVolume();
    };

    double get_surface(const OpenBabel::OBMol& molecule) {
        return get_sasa(molecule, 0);
    };

    double get_sasa(const OpenBabel::OBMol& molecule, double probe_radius) {
        if (molecule.NumAtoms() == 0) {
            return 0;
        };
        SurfaceIntegrator surface_integrator;
        for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
            OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
            OpenBabel::vector3 position = atom->GetVector();
            double radius = vdwlib::get_radius(atom) + probe_radius;
            Sphere sphere(position, radius);
            surface_integrator.add_sphere(sphere);
        };
        return surface_integrator.compute();
    };

    double get_ams(const OpenBabel::OBMol& molecule, std::vector<int> subset, double probe_radius) {
        if (molecule.NumAtoms() == 0) {
            return 0;
        };
        SurfaceIntegrator surface_integrator;
        for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
            OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
            OpenBabel::vector3 position = atom->GetVector();
            double radius = vdwlib::get_radius(atom) + probe_radius;
            Sphere sphere(position, radius);
            if (std::find(subset.begin(), subset.end(), i_atom) != subset.end()) {
                surface_integrator.add_sphere(sphere);
            } else {
                surface_integrator.add_inaccesible_volume_sphere(sphere);
            };
        };
        return surface_integrator.compute();
    };
};
