#define _USE_MATH_DEFINES
#include <cmath>
#include <algorithm>
#include "vdwlib/radii.hpp"
#include "vdwlib/descriptors/voxel-based.hpp"
#include "common.hpp"


namespace vdwlib::descriptors::voxel_based {
    static double _cur_grid_size(0.05);

    class OverlapIntegrationBox {
        public:
            OverlapIntegrationBox() : _grid_size(get_grid_size()) {
            };

            void add_sphere(unsigned group_id, vdwlib::Sphere& sphere) {
                if (group_id >= _spheres.size()) {
                    _spheres.resize(group_id + 1);
                };
                _spheres[group_id].push_back(sphere);
            };

            void add_sphere(unsigned group_id, OpenBabel::vector3& position,
                            double radius) {
                Sphere sphere(position, radius);
                add_sphere(group_id, sphere);
            };

            double compute() {
                _compute_boundary();
                long overlap_counter = 0;
                // Some versions of Open MP do not support unsigned indices
                int n_x = static_cast<int>(std::ceil((_boundary.max.x() - _boundary.min.x()) / _grid_size));
                int n_y = static_cast<int>(std::ceil((_boundary.max.y() - _boundary.min.y()) / _grid_size));
                int n_z = static_cast<int>(std::ceil((_boundary.max.z() - _boundary.min.z()) / _grid_size));
                #pragma omp parallel for reduction(+: overlap_counter) default(none) shared(n_x, n_y, n_z)
                for (int i_x = 0; i_x < n_x; ++i_x) {
                    for (int i_y = 0; i_y < n_y; ++i_y) {
                        for (int i_z = 0; i_z < n_z; ++i_z) {
                            double x = i_x * _grid_size + _boundary.min.x();
                            double y = i_y * _grid_size + _boundary.min.y();
                            double z = i_z * _grid_size + _boundary.min.z();
                            OpenBabel::vector3 grid_pos(x, y, z);
                            // Check if grid_pos is at least inside one sphere for each sphere group
                            if (std::all_of(_spheres.begin(), _spheres.end(),
                                [&](auto& sphere_group){
                                    return std::any_of(sphere_group.begin(), sphere_group.end(),
                                                       [&](auto& sphere){
                                                           return sphere.PositionInSphere(grid_pos);
                                                       });
                                })){
                                ++overlap_counter;
                            };
                        };
                    };
                };
                return overlap_counter * std::pow(_grid_size, 3);
            };

        protected:
            struct Boundary {
                OpenBabel::vector3 min;
                OpenBabel::vector3 max;
            };

            double _grid_size;
            std::vector<std::vector<vdwlib::Sphere>> _spheres;
            Boundary _boundary;

            inline void _compute_boundary() {
                _boundary = {
                    OpenBabel::vector3(_double_min, _double_min, _double_min),
                    OpenBabel::vector3(vdwlib::_double_max, vdwlib::_double_max,
                        vdwlib::_double_max)
                };
                std::vector<Boundary> tmp_boundaries(_spheres.size());
                for (auto& sphere_group : _spheres) { 
                    Boundary tmp_boundary = {
                        OpenBabel::vector3(vdwlib::_double_max, vdwlib::_double_max,
                            vdwlib::_double_max),
                        OpenBabel::vector3(vdwlib::_double_min, vdwlib::_double_min,
                            vdwlib::_double_min)
                    };
                    for (auto& sphere : sphere_group) {
                        OpenBabel::vector3 position = sphere.GetPosition();
                        double radius = sphere.GetRadius();
                        OpenBabel::vector3 offset(radius, radius, radius);
                        tmp_boundary.min = vdwlib::min(tmp_boundary.min, position
                                               - offset);
                        tmp_boundary.max = vdwlib::max(tmp_boundary.max, position
                                               + offset);
                    };
                    _boundary.min = vdwlib::max(_boundary.min, tmp_boundary.min);
                    _boundary.max = vdwlib::min(_boundary.max, tmp_boundary.max);
                };  
            };
    };

    bool set_grid_size(double grid_size) {
        if (grid_size > 0) {
            _cur_grid_size = grid_size;
            return true;
        } else {
            return false;
        };
    };

    double get_grid_size() {
        return _cur_grid_size;
    };

    double get_volume(const OpenBabel::OBMol& molecule) {
        if (molecule.NumAtoms() == 0) {
            return 0;
        };
        OverlapIntegrationBox overlap_integrator;
        for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
            OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
            OpenBabel::vector3 position = atom->GetVector();
            double radius = vdwlib::get_radius(atom);
            overlap_integrator.add_sphere(0, position, radius);
        };
        return overlap_integrator.compute();
    };

    double get_vdwo(const OpenBabel::OBMol& receptor,
                    const OpenBabel::OBMol& ligand) {
        if (ligand.NumAtoms() == 0 || receptor.NumAtoms() == 0) {
            return 0;
        };
        OverlapIntegrationBox overlap_integrator;
        for (unsigned group_id = 0; group_id < 2; ++group_id) {
            OpenBabel::OBMol molecule = (group_id == 0) ? receptor : ligand;
            for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
                OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
                OpenBabel::vector3 position = atom->GetVector();
                double radius = vdwlib::get_radius(atom);
                overlap_integrator.add_sphere(group_id, position, radius);
            };
        };
        return overlap_integrator.compute();
    };

    double get_burv_abs(const OpenBabel::OBMol& molecule,
            const OpenBabel::vector3 origin, double radius) {
        if (molecule.NumAtoms() == 0) {
            return 0;
        };
        OverlapIntegrationBox overlap_integrator;
        vdwlib::Sphere sphere(origin, radius);
        overlap_integrator.add_sphere(0, sphere);
        for (unsigned i_atom = 1; i_atom <= molecule.NumAtoms(); ++i_atom) {
            OpenBabel::OBAtom *atom = molecule.GetAtom(i_atom);
            OpenBabel::vector3 position = atom->GetVector();
            double radius = vdwlib::get_radius(atom);
            overlap_integrator.add_sphere(1, position, radius);
        };
        return overlap_integrator.compute();
    };

    double get_burv_abs(const OpenBabel::OBMol& molecule,
            unsigned origin, bool exclude_origin, double radius) {
        if (exclude_origin) {
            OpenBabel::OBMol copy = OpenBabel::OBMol(molecule);
            OpenBabel::OBAtom *atom = copy.GetAtom(origin);
            OpenBabel::vector3 position = atom->GetVector();
            copy.DeleteAtom(atom);
            return get_burv_abs(copy, position, radius);
        } else {
            OpenBabel::OBAtom *atom = molecule.GetAtom(origin);
            OpenBabel::vector3 position = atom->GetVector();
            return get_burv_abs(molecule, position, radius);
        };
    };

    double get_burv(const OpenBabel::OBMol& molecule,
            const OpenBabel::vector3 origin, double radius) {
        vdwlib::Sphere sphere(origin, radius);
        return 100. * get_burv_abs(molecule, origin, radius
                                   ) / sphere.GetVolume();
    };

    double get_burv(const OpenBabel::OBMol& molecule,
            unsigned origin, bool exclude_origin, double radius) {
        vdwlib::Sphere sphere(origin, radius);
        return 100. * get_burv_abs(molecule, origin, exclude_origin, radius
                                   ) / sphere.GetVolume();
    };
};

