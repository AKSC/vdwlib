#define _USE_MATH_DEFINES
#include <cmath>
#include <queue>
#include <openbabel/obiter.h>
#include <openbabel/atom.h>
#include <openbabel/mol.h>
#include <openbabel/bond.h>
#include "vdwlib/radii.hpp"
#include "vdwlib/descriptors/sterimol.hpp"
#include "common.hpp"

namespace vdwlib::descriptors {
    class Sterimol {
        public:
            Sterimol(OpenBabel::OBMol& molecule, unsigned i_atom_1, unsigned i_atom_2) :
                     _i_atom_1(i_atom_1), _i_atom_2(i_atom_2),
                     _molecule(molecule),
                     _atom_visited(_molecule.NumAtoms(), false) {
            };

            std::vector<double> compute(bool connectivity=false) {
                if (_i_atom_1 > _molecule.NumAtoms() || _i_atom_2 > _molecule.NumAtoms()) {
                    return std::vector<double> (3, 0);
                };
                _atom_1 = _molecule.GetAtom(_i_atom_1);
                _atom_2 = _molecule.GetAtom(_i_atom_2);
                _max_dist_par = 0;
                _min_dist_perp = vdwlib::_double_max;
                _max_dist_perp = 0;
                _origin = OpenBabel::vector3(_atom_1->GetVector());
                _ref_vector = OpenBabel::vector3(_atom_2->GetVector());
                _ref_vector -= _origin;
                _ref_vector.normalize();
                _ref_vector_perp_1 = vdwlib::gen_perp(_ref_vector);
                _ref_vector_perp_2 = OpenBabel::cross(_ref_vector_perp_1, _ref_vector);
                _rot_mat = OpenBabel::matrix3x3(_ref_vector_perp_1, _ref_vector_perp_2, _ref_vector);
                _atom_visited[_i_atom_1] = true;
                // Compute L, B1 and _perp_data
                if (connectivity) {
                    _graph(_atom_2);
                } else {
                    _all();
                };
                // Compute B5
                // std::vector<double> params;
                for (int i = 0; i < _perp_data.size(); ++i) {
                    for (int j = i + 1; j < _perp_data.size(); ++j) {
                        int i_1, i_2;
                        // set i_1 to atom with smaller vdW radius
                        if (std::get<1>(_perp_data[i]) > std::get<1>(_perp_data[j])) {
                            i_1 = j;
                            i_2 = i;
                        } else {
                            i_1 = i;
                            i_2 = j;
                        };
                        auto [pos_1, rad_1, idx_1] = _perp_data[i_1];
                        auto [pos_2, rad_2, idx_2] = _perp_data[i_2];
                        OpenBabel::vector3 _vect_1_2(pos_2);
                        _vect_1_2 -= pos_1;
                        OpenBabel::vector3 _vect_1_2_norm(_vect_1_2);
                        _vect_1_2_norm.normalize();
                        // Calculate outer tangents
                        double angle = std::asin((rad_2 - rad_1) / _vect_1_2.length());
                        angle *= 180 / M_PI;
                        for (int sign : {-1, 1}){
                            // Get new pos 1
                            _rot_mat.SetupRotMat(0, 0, sign * (angle + 90));
                            OpenBabel::vector3 vect_to_new_pos_1(_vect_1_2_norm);
                            vect_to_new_pos_1 *= _rot_mat;
                            vect_to_new_pos_1 *= rad_1;
                            OpenBabel::vector3 new_pos_1(pos_1);
                            new_pos_1 += vect_to_new_pos_1;
                            OpenBabel::vector3 vect_to_new_pos_2(_vect_1_2_norm);
                            vect_to_new_pos_2 *= _rot_mat;
                            vect_to_new_pos_2 *= rad_2;
                            OpenBabel::vector3 new_pos_2(pos_2);
                            new_pos_2 += vect_to_new_pos_2;
                            // Get point perpendicular to outer tangent
                            _rot_mat.SetupRotMat(0, 0, sign * angle);
                            OpenBabel::vector3 new_pos_perp(_vect_1_2_norm);
                            new_pos_perp *= _rot_mat;
                            new_pos_perp *= -OpenBabel::dot(new_pos_1, new_pos_perp);
                            new_pos_perp += new_pos_1;
                            double dist_perp = new_pos_perp.length();
                            if (std::isnan(dist_perp) || _min_dist_perp <= dist_perp) {
                                continue;
                            };
                            // Make sure that no other point is farther than the tangents
                            bool no_farther_points = true;
                            for (unsigned i_3=0; i_3<_perp_data.size(); ++i_3) {
                                if (i_3 == i_1 || i_3 == i_2) {
                                    continue;
                                };
                                auto [pos_3, rad_3, idx_3] = _perp_data[i_3];
                                OpenBabel::vector3 vect_to_pos_3(pos_3);
                                vect_to_pos_3 -= new_pos_perp;
                                double projected = OpenBabel::dot(vect_to_pos_3, new_pos_perp) / new_pos_perp.length();
                                projected += rad_3;
                                if (projected > 0) {
                                    no_farther_points = false;
                                    break;
                                };
                            };
                            if (no_farther_points) {
                                _min_dist_perp = dist_perp;
                                // params = {new_pos_1.GetX(), new_pos_1.GetY(), new_pos_perp.GetX(), new_pos_perp.GetY(), new_pos_2.GetX(), new_pos_2.GetY()};
                            };
                        };
                    };
                };
                // Add difference between C-H bond length and hydrogen vdW radius (1.50-1.10)
                _max_dist_par += 0.40;
                if (_min_dist_perp == _double_max) {
                    _min_dist_perp = _max_dist_perp;
                };
                return std::vector<double> {_max_dist_par, _min_dist_perp, _max_dist_perp};
                /*
                std::vector<double> out = {_max_dist_par, _min_dist_perp, _max_dist_perp};
                out.insert(out.end(), params.begin(), params.end());
                for (unsigned i=0; i<_perp_data.size(); ++i) {
                    auto [pos_1, rad_1, idx_1] = _perp_data[i];
                    out.push_back(pos_1.GetX());
                    out.push_back(pos_1.GetY());
                    out.push_back(rad_1);
                };
                return out;
                */
            };

        protected:
            double _max_dist_par;
            double _min_dist_perp;
            std::vector<std::tuple<OpenBabel::vector3,double,int>> _perp_data;
            double _max_dist_perp;
            OpenBabel::OBMol _molecule;
            unsigned _i_atom_1;
            unsigned _i_atom_2;
            OpenBabel::OBAtom *_atom_1;
            OpenBabel::OBAtom *_atom_2;
            OpenBabel::vector3 _origin;
            OpenBabel::vector3 _ref_vector;
            OpenBabel::vector3 _ref_vector_perp_1;
            OpenBabel::vector3 _ref_vector_perp_2;
            OpenBabel::matrix3x3 _rot_mat;
            std::vector<bool> _atom_visited;
            
            OpenBabel::vector3 _update_LB5(OpenBabel::OBAtom *atom) {
                // Get projected distances
                OpenBabel::vector3 vect_origin_atom(atom->GetVector());
                vect_origin_atom -= _origin;
                double dist_par = OpenBabel::dot(_ref_vector, vect_origin_atom);
                OpenBabel::vector3 vect_perp(_ref_vector);
                vect_perp *= -dist_par;
                vect_perp += vect_origin_atom;
                double dist_perp = vect_perp.length();
                // Rotate vector to perpendicular plane to get rid of the z coordinate
                vect_perp *= _rot_mat;
                // Account for vdW radius
                double radius = vdwlib::get_radius(atom);
                dist_par += radius;
                dist_perp += radius;
                // Update parameters 
                _max_dist_par = std::max(_max_dist_par, dist_par);
                _max_dist_perp = std::max(_max_dist_perp, dist_perp);
                return vect_perp;
            };

            void _all() {
                _perp_data.resize(_molecule.NumAtoms());
                for (int i_atom = 1; i_atom <= _molecule.NumAtoms(); ++i_atom) {
                    OpenBabel::OBAtom *atom = _molecule.GetAtom(i_atom);
                    double radius = vdwlib::get_radius(atom);
                    OpenBabel::vector3 vect_perp = _update_LB5(atom);
                    _perp_data[i_atom - 1] = std::make_tuple(vect_perp, radius, i_atom);
                };
            };

            void _graph(OpenBabel::OBAtom *atom) {
                // Breadth-first search algorithm
                if (_atom_visited[atom->GetIdx()]) {
                    return;
                };
                _atom_visited[atom->GetIdx()] = true;
                _perp_data.reserve(_molecule.NumAtoms());
                std::queue<OpenBabel::OBAtom *> front;
                front.push(atom);
                while (!front.empty()) {
                    atom = front.front();
                    front.pop();
                    FOR_NBORS_OF_ATOM(neighbor, atom) {
                        if (_atom_visited[neighbor->GetIdx()]) {
                            continue;
                        };
                        double radius = vdwlib::get_radius(neighbor);
                        OpenBabel::vector3 vect_perp = _update_LB5(&*neighbor);
                        _perp_data.push_back(std::make_tuple(vect_perp, radius, neighbor->GetIdx()));
                        _atom_visited[neighbor->GetIdx()] = true;
                        front.push(&*neighbor);
                    };
                };
            };
    };

    std::vector<double> get_sterimol(OpenBabel::OBMol& molecule,
                                     unsigned i_atom_1,
                                     unsigned i_atom_2,
                                     bool connectivity) {
        Sterimol sterimol(molecule, i_atom_1, i_atom_2);
        return sterimol.compute(connectivity);
    };
};
