#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <limits>
#include <openbabel/math/vector3.h>
#include <openbabel/math/matrix3x3.h>

// Reference: https://stackoverflow.com/a/33836073/15141722
#define COLLAPSED_TRIG_FOR(N, K) for (int K = 0; K < N * (N - 1) / 2; ++K)
#define COLLAPSED_TRIG_FOR_IDX_MAP(N, K, I, J) \
        int I = K / (N - 1); \
        int J = K % (N - 1); \
        if (J >= I) { \
            I = N - I - 1; \
            J = N - J - 2; \
        };

namespace vdwlib {
    static const double _double_min = -std::numeric_limits<double>::infinity();
    static const double _double_max = std::numeric_limits<double>::infinity();

    class Sphere {
        public:
            Sphere(OpenBabel::vector3 position, double radius) :
                   _position(position), _radius(radius) {
            };
            void SetPosition(const OpenBabel::vector3& position) {
                _position = position;
            };
            OpenBabel::vector3 GetPosition() const {
                return _position;
            };
            void SetRadius(double radius) {
                _radius = radius;
            };
            double GetRadius() {
                return _radius;
            };
            double GetSurface() const {
                return 4 * M_PI * std::pow(_radius, 2);
            };
            double GetVolume() const {
                return 4 * M_PI / 3 * std::pow(_radius, 3);
            };
            double PowerOfPosition(const OpenBabel::vector3& position) const {
                return _position.distSq(position) - std::pow(_radius, 2);
            };
            bool PositionInSphere(const OpenBabel::vector3& position) const {
                return PowerOfPosition(position) <= 0.;
            };
            bool PositionInSphere(const OpenBabel::vector3& position, const Sphere& other) const {
                double this_power = PowerOfPosition(position);
                if (this_power <= 0.) {
                    return this_power <= other.PowerOfPosition(position);
                } else {
                    return false;
                };
            };

        protected:
            OpenBabel::vector3 _position;
            double _radius;
    };

    class Cuboid {
        public:
            Cuboid(OpenBabel::vector3 position, OpenBabel::vector3 X,
                   OpenBabel::vector3 Y, OpenBabel::vector3 Z) :
                   _position(position), _vectors(OpenBabel::matrix3x3(X,Y,Z)) {
            };

            void SetPosition(OpenBabel::vector3 position) {
                _position = position;
            };
            OpenBabel::vector3& GetPosition() {
                return _position;
            };
            void SetVector(unsigned idx, OpenBabel::vector3 vector) {
                _vectors.SetRow(idx, vector);
            };
            OpenBabel::vector3 GetVector(unsigned idx) {
                return _vectors.GetRow(idx);
            };

        protected:
            OpenBabel::vector3 _position;
            OpenBabel::matrix3x3 _vectors;
    };

    OpenBabel::vector3 min(OpenBabel::vector3 a, OpenBabel::vector3 b);
    OpenBabel::vector3 max(OpenBabel::vector3 a, OpenBabel::vector3 b);
    OpenBabel::vector3 gen_perp(OpenBabel::vector3& inp);

    void print_progress_bar(double progress, std::ostream& stream = std::cout);
};

