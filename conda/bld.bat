cmake ^
      -G "NMake Makefiles" ^
      -DCMAKE_WINDOWS_EXPORT_ALL_SYMBOLS=ON ^
      -DCMAKE_BUILD_TYPE=Release ^
      -DCMAKE_PREFIX_PATH="%LIBRARY_PREFIX%" ^
      -DCMAKE_INSTALL_PREFIX=%LIBRARY_PREFIX% ^
      -DRUN_SWIG=ON ^
      -DPYTHON_BINDINGS=ON ^
      -DPYTHON_EXECUTABLE=%PYTHON% ^
      -DPython_FIND_REGISTRY=NEVER ^
      .
if errorlevel 1 exit 1

cmake --build . --target install --config Release
if errorlevel 1 exit 1
