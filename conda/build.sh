#!/usr/bin/env bash

cmake ${CMAKE_ARGS} \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_PREFIX_PATH="${PREFIX}" \
      -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
      -DRUN_SWIG=ON \
      -DPYTHON_BINDINGS=ON \
      -DPYTHON_EXECUTABLE="${PYTHON}" \
      .

make install
