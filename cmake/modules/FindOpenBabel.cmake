if(NOT OPENBABEL_INCLUDE_DIR)
    find_path(OPENBABEL_INCLUDE_DIR "openbabel/obconversion.h"
        "${OPENBABEL_DIR}/include/openbabel3"
        "${OPENBABEL_DIR}/include/openbabel-2.0"
        "$ENV{OPENBABEL_INCLUDE_DIR}/openbabel3"
        "$ENV{OPENBABEL_INCLUDE_DIR}/openbabel-2.0"
        "$ENV{OPENBABEL_DIR}/include/openbabel3"
        "$ENV{OPENBABEL_DIR}/include/openbabel-2.0"
        "$ENV{OPENBABEL_PATH}/include/openbabel3"
        "$ENV{OPENBABEL_PATH}/include/openbabel-2.0"
        "$ENV{OPENBABEL_BASE}/include/openbabel3"
        "$ENV{OPENBABEL_BASE}/include/openbabel-2.0"
        "${CMAKE_INSTALL_PREFIX}/include/openbabel3"
        "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0"
    )
    if(OPENBABEL_INCLUDE_DIR)
        # Update OPENBABEL_DIR
        get_filename_component(PARENT_DIR ${OPENBABEL_INCLUDE_DIR} DIRECTORY)
        get_filename_component(OPENBABEL_DIR ${PARENT_DIR} DIRECTORY)
        # Detect the version of Open Babel 
        get_filename_component(DIR_NAME ${OPENBABEL_INCLUDE_DIR} NAME)
        if("${DIR_NAME}" STREQUAL  "openbabel3")
            set(OPENBABEL_MAJOR_VERSION 3)
        elseif("${DIR_NAME}" STREQUAL  "openbabel-2.0")
            set(OPENBABEL_MAJOR_VERSION 2)
        else()
            message(WARNING "Could not detect the version of Open Babel")
        endif()
    endif()
endif()

if(OPENBABEL_INCLUDE_DIR AND NOT OPENBABEL_LIBRARIES)
    find_library(OPENBABEL_LIBRARIES NAMES openbabel3 openbabel-3 openbabel2 openbabel-2 openbabel
        HINTS
        "${OPENBABEL_DIR}/lib"
        "${OPENBABEL_DIR}/bin"
        "$ENV{OPENBABEL_LIBRARIES}"
    )
endif()

if(OPENBABEL_INCLUDE_DIR AND OPENBABEL_LIBRARIES)
    set(OpenBabel_FOUND TRUE)
else()
    if(OpenBabel_FIND_REQUIRED)
        message(FATAL_ERROR "Open Babel not found")
    endif()
endif()
