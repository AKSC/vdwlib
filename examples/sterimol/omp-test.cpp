#include <iostream>
#include <queue>
#include <omp.h>

int main(int argc, char** argv) {
    std::queue<int> vertex_queue = {1, 2, 3};

    #pragma omp parallel default(none) shared(q)
    {
        unsigned vertex;
        while (!vertex_queue.empty()) {
            #pragma omp critical
            {
                vertex = q.front();
                q.pop();
            };
            // Do something
            #pragma omp critical
            {
                vertex_queue.push(vertex);
            };
        };
    };
    return 0;
};
