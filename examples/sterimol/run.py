from os import environ, cpu_count
# Number of threads has to be set before loading vdwlib!
environ['OMP_NUM_THREADS'] = str(cpu_count())
from pathlib import Path
import tempfile
import vdwlib.descriptors
try:
    from openbabel import openbabel as ob
    obtable = ob
except ImportError:
    import openbabel as ob
    obtable = ob.OBElementTable()
try:
    from sterimol import sterimol
except ImportError:
    sterimol_loaded = False
else:
    sterimol_loaded = True

obconv = ob.OBConversion()
obbuild = ob.OBBuilder()

if __name__ == '__main__':
    mol = ob.OBMol()
    obconv.SetInFormat('smi')
    obconv.ReadString(mol, '[H][P]([CH3])([CH3])[CH3]')
    mol.AddHydrogens()
    obbuild.Build(mol)
    params = vdwlib.descriptors.get_sterimol(mol, 1, 2)
    print(params)
    if sterimol_loaded:
        with tempfile.NamedTemporaryFile(suffix='.com') as tmp_file:
            obconv.SetOutFormat('com')
            obconv.WriteFile(mol, tmp_file.name)
            params = sterimol.calcSterimol(tmp_file.name, 'bondi', 1, 2, False)
        print(params)
