from os import environ, cpu_count
# Number of threads has to be set before loading vdwlib!
environ['OMP_NUM_THREADS'] = str(cpu_count())
from pathlib import Path
import tempfile
import vdwlib.descriptors
try:
    from openbabel import openbabel as ob
    obtable = ob
except ImportError:
    import openbabel as ob
    obtable = ob.OBElementTable()
try:
    from sterimol import sterimol
except ImportError:
    sterimol_loaded = False
else:
    sterimol_loaded = True

obconv = ob.OBConversion()

if __name__ == '__main__':
    mol = ob.OBMol()
    obconv.SetInFormat('pdb')
    obconv.ReadFile(mol, '1evd.pdb')
    print(mol.NumAtoms())
    params = vdwlib.descriptors.get_sterimol(mol, 2, 1, True)
    print(params)
    if sterimol_loaded:
        with tempfile.NamedTemporaryFile(suffix='.com') as tmp_file:
            obconv.SetOutFormat('com')
            obconv.WriteFile(mol, tmp_file.name)
            params = sterimol.calcSterimol(tmp_file.name, 'bondi', 2, 1, False)
        print([params.lval, params.B1, params.newB5])
