import subprocess
import tempfile
from openbabel import openbabel as ob
import vdwlib
import vdwlib.descriptors.monte_carlo_based as mcb
import vdwlib.descriptors.voxel_based as vb


obconv = ob.OBConversion()

if __name__ == '__main__':
    obconv.SetInFormat('pdb')
    mol = ob.OBMol()
    obconv.ReadFile(mol, 'dafp1.pdb')
    with tempfile.NamedTemporaryFile(suffix='.msms') as tmp_file:
        with open(tmp_file.name, 'w') as stream:
            for atom in ob.OBMolAtomIter(mol):
                pos = atom.GetVector()
                stream.write(str(pos.GetX()))
                stream.write('\t')
                stream.write(str(pos.GetY()))
                stream.write('\t')
                stream.write(str(pos.GetZ()))
                stream.write('\t')
                stream.write(str(vdwlib.get_radius(atom)))
                stream.write('\n')
        proc = subprocess.Popen(['msms', '-if', tmp_file.name, '-probe_radius', '0.0001'])
        proc.communicate()
    print(mcb.get_sasa(mol, 0))
    print(mcb.get_volume(mol))
    print(vb.get_volume(mol))