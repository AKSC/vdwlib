#include <cstdio>
#include <filesystem>
#include <string>
#include <vector>
#include <openbabel/obconversion.h>
#include <openbabel/mol.h>
#include <vdwlib/descriptors.hpp>

static OpenBabel::OBConversion obconv;
static unsigned n_voxel = 2e4;

int main(int argc, char** argv) {
    if (argc < 2) {
        return 1;
    };
    std::vector<std::filesystem::path> paths(argv + 1, argv + argc);
    for (auto& path : paths) {
        OpenBabel::OBMol mol;
        std::string extension = path.extension();
        obconv.SetInFormat(extension.substr(1).c_str());
        obconv.ReadFile(&mol, path.string());
        float default_grid_size = vdwlib::descriptors::voxel_based::get_grid_size();
        vdwlib::descriptors::voxel_based::set_grid_size(1);
        float grid_size = vdwlib::descriptors::get_volume(mol) / n_voxel;
        if (grid_size < default_grid_size) {
            grid_size = default_grid_size;
        };
        vdwlib::descriptors::voxel_based::set_grid_size(grid_size);
        float volume = vdwlib::descriptors::get_volume(mol);
        std::printf("VdW Volume of %-16s%6.1f \u212b\u00b3\n", path.stem().c_str(), volume);
        std::fflush(stdout);
    };
    return 0;
};
