from os import environ, cpu_count
# Number of threads has to be set before loading vdwlib!
environ['OMP_NUM_THREADS'] = str(cpu_count())
from pathlib import Path
import vdwlib.descriptors
import vdwlib.descriptors.monte_carlo_based as mcb
try:
    from openbabel import openbabel as ob
except ImportError:
    import openbabel as ob

obconv = ob.OBConversion()

if __name__ == '__main__':
    root = Path(__file__).resolve().parent
    # Test molecules
    obconv.SetInFormat('mol')
    for path in (root / 'mols').glob('*.mol'):
        name = path.stem
        mol = ob.OBMol()
        obconv.ReadFile(mol, str(path))
        volume = vdwlib.descriptors.get_volume(mol)
        print(f'VdW Volume of {name+":":21s}{volume:6.1f} \u212b\u00b3')
        volume = mcb.get_volume(mol)
        print(f'VdW Volume of {name+":":21s}{volume:6.1f} \u212b\u00b3')
        area = mcb.get_surface(mol)
        print(f'VdW surface area of {name+":":15s}{area:6.1f} \u212b\u00b2')
    n_voxel = 2e4
    # Test proteins
    obconv.SetInFormat('pdb')
    for path in (root / 'mols').glob('*.pdb'):
        name = path.stem
        mol = ob.OBMol()
        obconv.ReadFile(mol, str(path))
        vdwlib.descriptors.voxel_based.set_grid_size(1)
        estimate = vdwlib.descriptors.get_volume(mol)
        vdwlib.descriptors.voxel_based.set_grid_size(estimate/n_voxel)
        volume = vdwlib.descriptors.get_volume(mol)
        print(f'VdW Volume of {name+":":21s}{volume:6.0f} \u212b\u00b3')
        volume = mcb.get_volume(mol)
        print(f'VdW Volume of {name+":":21s}{volume:6.0f} \u212b\u00b3')
        area = mcb.get_surface(mol)
        print(f'VdW surface area of {name+":":15s}{area:6.0f} \u212b\u00b2')