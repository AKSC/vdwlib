#pragma once
#include "vdwlib/descriptors/sterimol.hpp"
#include "vdwlib/descriptors/voxel-based.hpp"
namespace vdwlib::descriptors {
    using vdwlib::descriptors::voxel_based::get_volume;
    using vdwlib::descriptors::voxel_based::get_vdwo;
    using vdwlib::descriptors::voxel_based::get_burv;
    using vdwlib::descriptors::voxel_based::get_burv_abs;
};
