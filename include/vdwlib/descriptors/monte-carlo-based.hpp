#pragma once
#include <vector>
#include <openbabel/mol.h>
#include <openbabel/math/vector3.h>

namespace vdwlib::descriptors::monte_carlo_based {
    bool set_sample_size(long sample_size);
    long get_sample_size();

    bool set_seed(int seed);
    int get_seed();

    double get_volume(const OpenBabel::OBMol& molecule);

    double get_vdwo(const OpenBabel::OBMol& receptor,
        const OpenBabel::OBMol& ligand
    );

    double get_burv_abs(const OpenBabel::OBMol& molecule,
        const OpenBabel::vector3 origin, double radius=3.5);
    double get_burv_abs(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin=false, double radius=3.5
    );

    double get_burv(const OpenBabel::OBMol& molecule,
                    const OpenBabel::vector3 origin, double radius=3.5
    );
    double get_burv(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin=false, double radius=3.5
    );

    double get_surface(const OpenBabel::OBMol& molecule);

    double get_sasa(const OpenBabel::OBMol& molecule, double probe_radius=1.4);

    double get_ams(const OpenBabel::OBMol& molecule, std::vector<int> subset, double probe_radius=1.4);
};
