#pragma once
#include <vector>
#include <openbabel/math/vector3.h>
#include <openbabel/math/matrix3x3.h>
#include <openbabel/atom.h>
#include <openbabel/mol.h>

namespace vdwlib::descriptors {
    std::vector<double> get_sterimol(OpenBabel::OBMol& molecule,
                                     unsigned i_atom_1,
                                     unsigned i_atom_2,
                                     bool connectivity=true);
};

