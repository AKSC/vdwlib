#pragma once
#include <openbabel/mol.h>
#include <openbabel/math/vector3.h>

namespace vdwlib::descriptors::voxel_based {
    bool set_grid_size(double grid_size);
    double get_grid_size();

    double get_volume(const OpenBabel::OBMol& molecule);

    double get_vdwo(const OpenBabel::OBMol& receptor,
        const OpenBabel::OBMol& ligand);

    double get_burv_abs(const OpenBabel::OBMol& molecule,
        const OpenBabel::vector3 origin, double radius=3.5);

    double get_burv_abs(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin=false, double radius=3.5);

    double get_burv(const OpenBabel::OBMol& molecule,
                    const OpenBabel::vector3 origin, double radius=3.5);

    double get_burv(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin=false, double radius=3.5);
};
