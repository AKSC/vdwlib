#pragma once
#include <string>
#include <openbabel/atom.h>

namespace vdwlib {
    bool set_radii_source(std::string radii_src, bool use_bondi_h=false);
    std::string get_radii_source();
    std::vector<std::string> get_radii_sources();
    bool use_bondi_h();
    bool set_radii_scale(double scale);
    double get_radii_scale();
    double get_radius(unsigned atomic_num);
    double get_radius(const OpenBabel::OBAtom& atom);
    double get_radius(const OpenBabel::OBAtom *atom);
};
