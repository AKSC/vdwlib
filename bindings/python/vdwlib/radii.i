%module(docstring="Van der Waals radii module") radii
%include "std_string.i"
%include "std_vector.i"
%{
#define SWIG_FILE_WITH_INIT
#include <string>
#include <vector>
#include <openbabel/atom.h>
#include "vdwlib/radii.hpp"
%}

%pythoncode %{__all__ = [
    'set_radii_source',
    'get_radii_source',
    'get_radii_sources',
    'use_bondi_h',
    'set_radii_scale',
    'get_radii_scale',
    'get_radius'
]%}

namespace vdwlib {
    %feature("docstring") set_radii_source %{
        Changes the source for the van der Waals radii.

        set_radii_source(radii_src: str, use_bondi_h: bool = False) -> bool

        :param radii_src: name of the source
        :param use_bondi_h: if true, the radius for hydrogen is set to 1.20 Å
        otherwise 1.09 or 1.10 Å is used.
        :return: True if source could be set.
    %}
    bool set_radii_source(std::string radii_src, bool use_bondi_h=false);

    %feature("docstring") get_radii_source %{
        Get the name of the current van der Waals radii source.

        get_radii_source() -> str

        :return: source name
    %}
    std::string get_radii_source();

    %feature("docstring") get_radii_sources %{
        Get the name of all available van der Waals radii sources.

        get_radii_sources() -> list[str]

        :return: source names
    %}
    %feature("pythonappend") get_radii_sources %{val = list(val)%}
    std::vector<std::string> get_radii_sources();

    %feature("docstring") use_bondi_h %{
        Checks if the the Bondi radius is used for hydrogen (1.20 Å).

        use_bondi_h() -> bool

        :return: True if Bondi radius is used
    %}
    bool use_bondi_h();

    %feature("docstring") set_radii_scale %{
        Changes the scale for the van der Waals radii.

        set_radii_scale(scale: float) -> bool

        :param scale: the new radii scale
        :return: True if scale could be set.
    %}
    bool set_radii_scale(double scale);

    %feature("docstring") get_radii_scale %{
        Returns the currently used radii scale.

        get_radii_scale() -> float

        :return: radii scale
    %}
    double get_radii_scale();

    %feature("docstring") get_radius %{
        Returns the van der Waals radius for an atom or element number.

        get_radius(atomic_num: int) -> float

        :param atomic_num: element number
        :return: returns van der Waals radius in Å

        get_radius(atom: openbabel.OBAtom) -> float

        :param atom: atom
        :return: returns van der Waals radius in Å
    %}
    double get_radius(unsigned atomic_num);
    double get_radius(const OpenBabel::OBAtom& atom);
    // double get_radius(const OpenBabel::OBAtom *atom);
};

