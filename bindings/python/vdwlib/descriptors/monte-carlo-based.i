%module(docstring="Monte-Carlo-based descriptors module") monte_carlo_based
%include "std_vector.i"
%{
#define SWIG_FILE_WITH_INIT
#include <vector>
#include <openbabel/mol.h>
#include <openbabel/math/vector3.h>
#include "vdwlib/descriptors/monte-carlo-based.hpp"
%}
%template(IntVector) std::vector<int>;

%pythoncode %{__all__ = [
    'set_sample_size',
    'get_sample_size',
    'set_seed',
    'get_seed',
    'get_volume',
    'get_vdwo',
    'get_burv_abs',
    'get_burv',
    'get_surface',
    'get_sasa',
    'get_ams'
]%}

namespace vdwlib::descriptors::monte_carlo_based {        
    %feature("docstring") set_sample_size %{
        Changes the number of sample points for Monte Carlo.

        set_sample_size(sample_size: int) -> bool

        :param sample_size: new number of sample points
        :return: True if sample points could be set.
    %}
    bool set_sample_size(long sample_size);

    %feature("docstring") get_sample_size %{
        Returns the number of sample points for Monte Carlo.

        get_sample_size() -> int

        :return: sample size
    %}
    long get_sample_size();

    %feature("docstring") set_seed %{
        Changes seed for Monte Carlo.

        set_seed(seed: int) -> bool

        :param seed: the new seed value
        :return: True if seed could be set.
    %}
    bool set_seed(int seed);

    %feature("docstring") get_seed %{
        Returns the seed for Monte Carlo.

        get_seed() -> int

        :return: seed
    %}
    int get_seed();

    %feature("docstring") get_volume %{
        Calculates the van der Waals volume via numeric integration.

        get_volume(molecule: openbabel.OBMol) -> float

        :param molecule: the structure
        :return: returns volume in \u212b\u00b3
    %}
    double get_volume(const OpenBabel::OBMol& molecule);

    %feature("docstring") get_vdwo %{
        Calculates the van der Waals overlap volume between two structures via

        get_vdwo(receptor: openbabel.OBMol, ligand: openbabel.OBMol) -> float

        numeric integration.
        :param receptor: the first structure
        :param ligand: the second structure
        :return: returns volume in \u212b\u00b3
    %}
    double get_vdwo(const OpenBabel::OBMol& receptor,
            const OpenBabel::OBMol& ligand);

    %feature("docstring") get_burv_abs %{
        Calculates V_bur based on the specified sphere via numeric integration.

        get_burv_abs(molecule: openbabel.OBMol, origin: openbabel.vector3,
            radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: origin of the sphere in \u212b
        :param radius: radius of the sphere in \u212b
        :return: returns V_bur

        get_burv(molecule: openbabel.OBMol, origin: int,
            exclude_origin: bool = False, radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: Index of the atom on which the sphere is placed.
        :param exclude_origin: Exclude origin from the calculation.
        :param radius: radius of the sphere in \u212b
        :return: returns V_bur
    %}
    double get_burv_abs(const OpenBabel::OBMol& molecule,
        OpenBabel::vector3 origin, double radius=3.5
    );
    double get_burv_abs(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin=false, double radius=3.5
    );

    %feature("docstring") get_burv %{
        Calculates %V_bur based on the specified sphere via numeric integration.

        get_burv(molecule: openbabel.OBMol, origin: openbabel.vector3,
                 radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: origin of the sphere in \u212b
        :param radius: radius of the sphere in \u212b
        :return: returns %V_bur

        get_burv(molecule: openbabel.OBMol, origin: int,
            exclude_origin: bool = False, radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: Index of the atom on which the sphere is placed.
        :param exclude_origin: Exclude origin from the calculation.
        :param radius: radius of the sphere in \u212b
        :return: returns %V_bur
    %}
    double get_burv(const OpenBabel::OBMol& molecule,
        OpenBabel::vector3 origin, double radius=3.5
    );
    double get_burv(const OpenBabel::OBMol& molecule,
        unsigned origin, bool exclude_origin=false, double radius=3.5
    );

    %feature("docstring") get_surface %{
        Calculates surfce area based on Monte Carlo integration.

        get_surface(molecule: openbabel.OBMol) -> float

        :param molecule: the structure
        :return: returns Surface area in \u212b\u00b2
    %}
    double get_surface(const OpenBabel::OBMol& molecule);

    %feature("docstring") get_sasa %{
        Calculates solvent accessible surface area based on  Monte Carlo integration.

        get_sasa(molecule: openbabel.OBMol, probe_radius: float = 1.4) -> float

        :param molecule: the structure
        :return: returns Surface area in \u212b\u00b2
    %}
    double get_sasa(const OpenBabel::OBMol& molecule, double probe_radius=1.4);

    %feature("docstring") get_ams %{
        Calculates solvent accessible surface area based on  Monte Carlo integration.

        get_ams(molecule: openbabel.OBMol, list[int]: subset, probe_radius: float = 1.4) -> float

        :param molecule: the structure
        :return: returns Surface area in \u212b\u00b2
    %}
    double get_ams(const OpenBabel::OBMol& molecule, std::vector<int> subset, double probe_radius=1.4);
};
