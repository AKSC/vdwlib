%module(docstring="Sterimol descriptor module") sterimol
%include "std_vector.i"
%{
#define SWIG_FILE_WITH_INIT
#include <vector>
#include <openbabel/mol.h>
#include "vdwlib/descriptors/sterimol.hpp"
%}

%pythoncode %{__all__ = [
    'get_sterimol'
]%}

namespace vdwlib::descriptors {
    %feature("docstring") get_sterimol %{
        Calculates sterimol parameters L, B1 and B5.

        get_sterimol(molecule: openbabel.OBMol, i_atom_1: int, i_atom_2: int,
                connectivity: bool = True) -> list[float]

        L:  Estimates length of substituent from atom_1 in direction of atom_2
        B1: Estimates the minimal width of the substituent in the plane
            perpendicular to the line between atom_1 and atom_2.
        B5: Same as B1, but maximum width instead.
        :param molecule: the structure
        :param i_atom_1: index of first atom
        :param i_atom_2: index of second atom
        :param connectivity: If true, only atoms in a subsitutent connected
                             to atom_2 will be considered (excluding atom_1).
        :return: returns [L, B1, B5]
    %}
    %feature("pythonappend") get_sterimol %{val = list(val)%}
    std::vector<double> get_sterimol(OpenBabel::OBMol& molecule,
            unsigned i_atom_1, unsigned i_atom_2, bool connectivity=true);
};
