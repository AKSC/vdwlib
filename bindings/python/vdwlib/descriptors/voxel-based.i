%module(docstring="Voxel-based descriptors module") voxel_based
%{
#define SWIG_FILE_WITH_INIT
#include <openbabel/mol.h>
#include <openbabel/math/vector3.h>
#include "vdwlib/descriptors/voxel-based.hpp"
%}

%pythoncode %{__all__ = [
    'set_grid_size',
    'get_grid_size',
    'get_volume',
    'get_vdwo',
    'get_burv_abs',
    'get_burv'
]%}

namespace vdwlib::descriptors::voxel_based {        
    %feature("docstring") set_grid_size %{
        Changes the grid size for the voxels.

        set_grid_size(grid_size: float) -> bool

        :param grid_size: grid size for the numeric integration in \u212b
        :return: True if source could be set.
    %}
    bool set_grid_size(double grid_size);

    %feature("docstring") get_grid_size %{
        Returns the grid size for the voxels.

        get_grid_size() -> float

        :return: grid size
    %}
    double get_grid_size();

    %feature("docstring") get_volume %{
        Calculates the van der Waals volume via numeric integration.

        get_volume(molecule: openbabel.OBMol) -> float

        :param molecule: the structure
        :return: returns volume in \u212b\u00b3
    %}
    double get_volume(const OpenBabel::OBMol& molecule);

    %feature("docstring") get_vdwo %{
        Calculates the van der Waals overlap volume between two structures via

        get_vdwo(receptor: openbabel.OBMol, ligand: openbabel.OBMol) -> float

        numeric integration.
        :param receptor: the first structure
        :param ligand: the second structure
        :return: returns volume in \u212b\u00b3
    %}
    double get_vdwo(const OpenBabel::OBMol& receptor,
            const OpenBabel::OBMol& ligand);

    %feature("docstring") get_burv_abs %{
        Calculates V_bur based on the specified sphere via numeric integration.

        get_burv_abs(molecule: openbabel.OBMol, origin: openbabel.vector3,
                radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: origin of the sphere in \u212b
        :param radius: radius of the sphere in \u212b
        :return: returns V_bur

        get_burv(molecule: openbabel.OBMol, origin: int,
                exclude_origin: bool = False, radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: Index of the atom on which the sphere is placed.
        :param exclude_origin: Exclude origin from the calculation.
        :param radius: radius of the sphere in \u212b
        :return: returns V_bur
    %}
    double get_burv_abs(const OpenBabel::OBMol& molecule,
            OpenBabel::vector3 origin, double radius=3.5);
    double get_burv_abs(const OpenBabel::OBMol& molecule,
            unsigned origin, bool exclude_origin=false, double radius=3.5);

    %feature("docstring") get_burv %{
        Calculates %V_bur based on the specified sphere via numeric integration.

        get_burv(molecule: openbabel.OBMol, origin: openbabel.vector3,
                 radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: origin of the sphere in \u212b
        :param radius: radius of the sphere in \u212b
        :return: returns %V_bur

        get_burv(molecule: openbabel.OBMol, origin: int,
                exclude_origin: bool = False, radius: float = 3.5) -> float

        :param molecule: the structure
        :param origin: Index of the atom on which the sphere is placed.
        :param exclude_origin: Exclude origin from the calculation.
        :param radius: radius of the sphere in \u212b
        :return: returns %V_bur
    %}
    double get_burv(const OpenBabel::OBMol& molecule,
            OpenBabel::vector3 origin, double radius=3.5);
    double get_burv(const OpenBabel::OBMol& molecule,
            unsigned origin, bool exclude_origin=false, double radius=3.5);
};

